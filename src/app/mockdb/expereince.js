export const EXPERIENCE = [
  {
    time: "OCT 2020 - CURRENT",
    text: `Front end developer at buzzebees co. ltd.
    (thailand). Coding program from design by
    designer and connect API from backend
    to control CRM privilege by use Vue.js`
  },
  {
    time: "APR 2020 – SEP 2020",
    text: `IT Specialist at Mitsubishi Motors Thailand
    Co ,ltd. Full stack developing for control
    confirmation of send date of sending part
    between supplier and purchaser by use
    ASP.NET core C#, SQL , HTML, Angular 7
    and CSS`
  },
  {
    time: "AUG 2018 - APR 2020",
    text: `IT-Specialist at Celestica (Thailand) Ltd. Full
    stack developing for control assembling in
    line and system about e-leaving by use
    ASP.NET core C#, SQL , HTML, Angular JS ,
    Angular 2+ , JAVASCRIPT, CSS and jQuery`
  },
  {
    time: "JUL 2019",
    text: `Frontend developer at TOP CHAROEN.
    Developing system to control sell process
    at front shop for order, developing by use
    Angular 7, HTML, JAVASCRIPT and CSS`
  },
  {
    time: "OCT 2016 - JUL 2018",
    text: `Software Analysis & Full Stack developer
    at Siam Compressor Industry Co., Ltd. in
    Mitsubishi Electric group. My responsibility
    is setting full loop of SDLC in each system,
    developing by use ASP.NET (VB or C#),
    SQL, HTML, JAVASCRIPT, CSS and jQuery`
  },
  {
    time: "JAN 2016 – SEP 2016",
    text: `Backend developer at Computerlogy Co.,
    Ltd. (Ruby on Rails)`
  }
];

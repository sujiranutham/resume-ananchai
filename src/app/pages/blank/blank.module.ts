import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BankRoutingModule } from './bank-routing.module';
import { WelcomeComponent } from './welcome/welcome.component';
import { MaterialModule } from 'src/app/core/material.module';
@NgModule({
  declarations: [WelcomeComponent],
  imports: [
    CommonModule,
    BankRoutingModule,
    MaterialModule
  ]
})
export class BlankModule {

}

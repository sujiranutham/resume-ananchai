import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainRoutingModule } from './main-routing.module';
import { ResumeComponent } from './resume/resume.component';
import { LoadingComponent } from "./loading/loading.component";



@NgModule({
  declarations: [ResumeComponent,LoadingComponent],
  imports: [
    CommonModule,
    MainRoutingModule
  ]
})
export class MainModule { }

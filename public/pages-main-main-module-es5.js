function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-main-main-module"], {
  /***/
  "./src/app/mockdb/expereince.js":
  /*!**************************************!*\
    !*** ./src/app/mockdb/expereince.js ***!
    \**************************************/

  /*! exports provided: EXPERIENCE */

  /***/
  function srcAppMockdbExpereinceJs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EXPERIENCE", function () {
      return EXPERIENCE;
    });

    var EXPERIENCE = [{
      time: "OCT 2020 - CURRENT",
      text: "Front end developer at buzzebees co. ltd.\n    (thailand). Coding program from design by\n    designer and connect API from backend\n    to control CRM privilege by use Vue.js"
    }, {
      time: "APR 2020 – SEP 2020",
      text: "IT Specialist at Mitsubishi Motors Thailand\n    Co ,ltd. Full stack developing for control\n    confirmation of send date of sending part\n    between supplier and purchaser by use\n    ASP.NET core C#, SQL , HTML, Angular 7\n    and CSS"
    }, {
      time: "AUG 2018 - APR 2020",
      text: "IT-Specialist at Celestica (Thailand) Ltd. Full\n    stack developing for control assembling in\n    line and system about e-leaving by use\n    ASP.NET core C#, SQL , HTML, Angular JS ,\n    Angular 2+ , JAVASCRIPT, CSS and jQuery"
    }, {
      time: "JUL 2019",
      text: "Frontend developer at TOP CHAROEN.\n    Developing system to control sell process\n    at front shop for order, developing by use\n    Angular 7, HTML, JAVASCRIPT and CSS"
    }, {
      time: "OCT 2016 - JUL 2018",
      text: "Software Analysis & Full Stack developer\n    at Siam Compressor Industry Co., Ltd. in\n    Mitsubishi Electric group. My responsibility\n    is setting full loop of SDLC in each system,\n    developing by use ASP.NET (VB or C#),\n    SQL, HTML, JAVASCRIPT, CSS and jQuery"
    }, {
      time: "JAN 2016 – SEP 2016",
      text: "Backend developer at Computerlogy Co.,\n    Ltd. (Ruby on Rails)"
    }];
    /***/
  },

  /***/
  "./src/app/pages/main/loading/loading.component.ts":
  /*!*********************************************************!*\
    !*** ./src/app/pages/main/loading/loading.component.ts ***!
    \*********************************************************/

  /*! exports provided: LoadingComponent */

  /***/
  function srcAppPagesMainLoadingLoadingComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LoadingComponent", function () {
      return LoadingComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

    var LoadingComponent = /*#__PURE__*/function () {
      function LoadingComponent() {
        _classCallCheck(this, LoadingComponent);
      }

      _createClass(LoadingComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return LoadingComponent;
    }();

    LoadingComponent.ɵfac = function LoadingComponent_Factory(t) {
      return new (t || LoadingComponent)();
    };

    LoadingComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: LoadingComponent,
      selectors: [["app-loading"]],
      decls: 9,
      vars: 0,
      consts: [[1, "full-screen"], [1, "loading"], [1, "lds-ripple"], [1, "text"], [1, "text", "small", "mt-1"]],
      template: function LoadingComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "PLEASE WAIT");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "now system loading my resume");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      },
      styles: [".full-screen[_ngcontent-%COMP%] {\n  position: fixed;\n  top: 0;\n  left: 0;\n  width: 100vw;\n  height: 100vh;\n  z-index: 999;\n  background: #2980b9;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n.full-screen[_ngcontent-%COMP%]   .loading[_ngcontent-%COMP%] {\n  width: auto;\n  height: 200px;\n}\n.full-screen[_ngcontent-%COMP%]   .loading[_ngcontent-%COMP%]   .text[_ngcontent-%COMP%] {\n  text-transform: uppercase;\n  text-align: center;\n  font-size: 26px;\n  color: #fff;\n  font-weight: bold;\n  letter-spacing: 2px;\n}\n.full-screen[_ngcontent-%COMP%]   .loading[_ngcontent-%COMP%]   .small[_ngcontent-%COMP%] {\n  font-size: 18px;\n}\n.lds-ripple[_ngcontent-%COMP%] {\n  position: relative;\n  margin: 0 auto;\n  width: 80px;\n  height: 80px;\n}\n.lds-ripple[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n  position: absolute;\n  border: 4px solid #fff;\n  opacity: 1;\n  border-radius: 50%;\n  -webkit-animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;\n          animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;\n}\n.lds-ripple[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]:nth-child(2) {\n  -webkit-animation-delay: -0.5s;\n          animation-delay: -0.5s;\n}\n@-webkit-keyframes lds-ripple {\n  0% {\n    top: 36px;\n    left: 36px;\n    width: 0;\n    height: 0;\n    opacity: 1;\n  }\n  100% {\n    top: 0px;\n    left: 0px;\n    width: 72px;\n    height: 72px;\n    opacity: 0;\n  }\n}\n@keyframes lds-ripple {\n  0% {\n    top: 36px;\n    left: 36px;\n    width: 0;\n    height: 0;\n    opacity: 1;\n  }\n  100% {\n    top: 0px;\n    left: 0px;\n    width: 72px;\n    height: 72px;\n    opacity: 0;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWFpbi9sb2FkaW5nL0M6XFxVc2Vyc1xcYW5hbmNoYWlzXFxEZXNrdG9wXFxBcHBDb2RlXFxyZXN1bWVcXHJlc3VtZS1kZW1vL3NyY1xcYXBwXFxwYWdlc1xcbWFpblxcbG9hZGluZ1xcbG9hZGluZy5jb21wb25lbnQuc2FzcyIsInNyYy9hcHAvcGFnZXMvbWFpbi9sb2FkaW5nL2xvYWRpbmcuY29tcG9uZW50LnNhc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxlQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FDQ0Y7QURDRTtFQUNFLFdBQUE7RUFDQSxhQUFBO0FDQ0o7QURDSTtFQUNFLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUNDTjtBREFJO0VBQ0UsZUFBQTtBQ0VOO0FEQUE7RUFDRSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQ0dGO0FEREE7RUFDRSxrQkFBQTtFQUNBLHNCQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0VBQ0Esc0VBQUE7VUFBQSw4REFBQTtBQ0lGO0FERkE7RUFDRSw4QkFBQTtVQUFBLHNCQUFBO0FDS0Y7QURIQTtFQUNFO0lBQ0UsU0FBQTtJQUNBLFVBQUE7SUFDQSxRQUFBO0lBQ0EsU0FBQTtJQUNBLFVBQUE7RUNNRjtFREpBO0lBQ0UsUUFBQTtJQUNBLFNBQUE7SUFDQSxXQUFBO0lBQ0EsWUFBQTtJQUNBLFVBQUE7RUNNRjtBQUNGO0FEcEJBO0VBQ0U7SUFDRSxTQUFBO0lBQ0EsVUFBQTtJQUNBLFFBQUE7SUFDQSxTQUFBO0lBQ0EsVUFBQTtFQ01GO0VESkE7SUFDRSxRQUFBO0lBQ0EsU0FBQTtJQUNBLFdBQUE7SUFDQSxZQUFBO0lBQ0EsVUFBQTtFQ01GO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9tYWluL2xvYWRpbmcvbG9hZGluZy5jb21wb25lbnQuc2FzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mdWxsLXNjcmVlblxyXG4gIHBvc2l0aW9uOiBmaXhlZFxyXG4gIHRvcDogMFxyXG4gIGxlZnQ6IDBcclxuICB3aWR0aDogMTAwdndcclxuICBoZWlnaHQ6IDEwMHZoXHJcbiAgei1pbmRleDogOTk5XHJcbiAgYmFja2dyb3VuZDogIzI5ODBiOVxyXG4gIGRpc3BsYXk6IGZsZXhcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlclxyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXJcclxuXHJcbiAgLmxvYWRpbmdcclxuICAgIHdpZHRoOiBhdXRvXHJcbiAgICBoZWlnaHQ6IDIwMHB4XHJcblxyXG4gICAgLnRleHRcclxuICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZVxyXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXJcclxuICAgICAgZm9udC1zaXplOiAyNnB4XHJcbiAgICAgIGNvbG9yOiAjZmZmXHJcbiAgICAgIGZvbnQtd2VpZ2h0OiBib2xkXHJcbiAgICAgIGxldHRlci1zcGFjaW5nOiAycHhcclxuICAgIC5zbWFsbCBcclxuICAgICAgZm9udC1zaXplOiAxOHB4XHJcblxyXG4ubGRzLXJpcHBsZVxyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZVxyXG4gIG1hcmdpbjogMCBhdXRvXHJcbiAgd2lkdGg6IDgwcHhcclxuICBoZWlnaHQ6IDgwcHhcclxuXHJcbi5sZHMtcmlwcGxlIGRpdlxyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZVxyXG4gIGJvcmRlcjogNHB4IHNvbGlkICNmZmZcclxuICBvcGFjaXR5OiAxXHJcbiAgYm9yZGVyLXJhZGl1czogNTAlXHJcbiAgYW5pbWF0aW9uOiBsZHMtcmlwcGxlIDFzIGN1YmljLWJlemllcigwLCAwLjIsIDAuOCwgMSkgaW5maW5pdGVcclxuXHJcbi5sZHMtcmlwcGxlIGRpdjpudGgtY2hpbGQoMilcclxuICBhbmltYXRpb24tZGVsYXk6IC0wLjVzXHJcblxyXG5Aa2V5ZnJhbWVzIGxkcy1yaXBwbGVcclxuICAwJVxyXG4gICAgdG9wOiAzNnB4XHJcbiAgICBsZWZ0OiAzNnB4XHJcbiAgICB3aWR0aDogMFxyXG4gICAgaGVpZ2h0OiAwXHJcbiAgICBvcGFjaXR5OiAxXHJcblxyXG4gIDEwMCVcclxuICAgIHRvcDogMHB4XHJcbiAgICBsZWZ0OiAwcHhcclxuICAgIHdpZHRoOiA3MnB4XHJcbiAgICBoZWlnaHQ6IDcycHhcclxuICAgIG9wYWNpdHk6IDBcclxuIiwiLmZ1bGwtc2NyZWVuIHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIHdpZHRoOiAxMDB2dztcbiAgaGVpZ2h0OiAxMDB2aDtcbiAgei1pbmRleDogOTk5O1xuICBiYWNrZ3JvdW5kOiAjMjk4MGI5O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5mdWxsLXNjcmVlbiAubG9hZGluZyB7XG4gIHdpZHRoOiBhdXRvO1xuICBoZWlnaHQ6IDIwMHB4O1xufVxuLmZ1bGwtc2NyZWVuIC5sb2FkaW5nIC50ZXh0IHtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDI2cHg7XG4gIGNvbG9yOiAjZmZmO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgbGV0dGVyLXNwYWNpbmc6IDJweDtcbn1cbi5mdWxsLXNjcmVlbiAubG9hZGluZyAuc21hbGwge1xuICBmb250LXNpemU6IDE4cHg7XG59XG5cbi5sZHMtcmlwcGxlIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW46IDAgYXV0bztcbiAgd2lkdGg6IDgwcHg7XG4gIGhlaWdodDogODBweDtcbn1cblxuLmxkcy1yaXBwbGUgZGl2IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBib3JkZXI6IDRweCBzb2xpZCAjZmZmO1xuICBvcGFjaXR5OiAxO1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGFuaW1hdGlvbjogbGRzLXJpcHBsZSAxcyBjdWJpYy1iZXppZXIoMCwgMC4yLCAwLjgsIDEpIGluZmluaXRlO1xufVxuXG4ubGRzLXJpcHBsZSBkaXY6bnRoLWNoaWxkKDIpIHtcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC41cztcbn1cblxuQGtleWZyYW1lcyBsZHMtcmlwcGxlIHtcbiAgMCUge1xuICAgIHRvcDogMzZweDtcbiAgICBsZWZ0OiAzNnB4O1xuICAgIHdpZHRoOiAwO1xuICAgIGhlaWdodDogMDtcbiAgICBvcGFjaXR5OiAxO1xuICB9XG4gIDEwMCUge1xuICAgIHRvcDogMHB4O1xuICAgIGxlZnQ6IDBweDtcbiAgICB3aWR0aDogNzJweDtcbiAgICBoZWlnaHQ6IDcycHg7XG4gICAgb3BhY2l0eTogMDtcbiAgfVxufSJdfQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](LoadingComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-loading',
          templateUrl: './loading.component.html',
          styleUrls: ['./loading.component.sass']
        }]
      }], function () {
        return [];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/pages/main/main-routing.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/pages/main/main-routing.module.ts ***!
    \***************************************************/

  /*! exports provided: MainRoutingModule */

  /***/
  function srcAppPagesMainMainRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MainRoutingModule", function () {
      return MainRoutingModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _resume_resume_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./resume/resume.component */
    "./src/app/pages/main/resume/resume.component.ts");

    var routes = [{
      path: '',
      component: _resume_resume_component__WEBPACK_IMPORTED_MODULE_2__["ResumeComponent"]
    }];

    var MainRoutingModule = function MainRoutingModule() {
      _classCallCheck(this, MainRoutingModule);
    };

    MainRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
      type: MainRoutingModule
    });
    MainRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
      factory: function MainRoutingModule_Factory(t) {
        return new (t || MainRoutingModule)();
      },
      imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](MainRoutingModule, {
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MainRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
          declarations: [],
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/pages/main/main.module.ts":
  /*!*******************************************!*\
    !*** ./src/app/pages/main/main.module.ts ***!
    \*******************************************/

  /*! exports provided: MainModule */

  /***/
  function srcAppPagesMainMainModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MainModule", function () {
      return MainModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _main_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./main-routing.module */
    "./src/app/pages/main/main-routing.module.ts");
    /* harmony import */


    var _resume_resume_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./resume/resume.component */
    "./src/app/pages/main/resume/resume.component.ts");
    /* harmony import */


    var _loading_loading_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./loading/loading.component */
    "./src/app/pages/main/loading/loading.component.ts");

    var MainModule = function MainModule() {
      _classCallCheck(this, MainModule);
    };

    MainModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
      type: MainModule
    });
    MainModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
      factory: function MainModule_Factory(t) {
        return new (t || MainModule)();
      },
      imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _main_routing_module__WEBPACK_IMPORTED_MODULE_2__["MainRoutingModule"]]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](MainModule, {
        declarations: [_resume_resume_component__WEBPACK_IMPORTED_MODULE_3__["ResumeComponent"], _loading_loading_component__WEBPACK_IMPORTED_MODULE_4__["LoadingComponent"]],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _main_routing_module__WEBPACK_IMPORTED_MODULE_2__["MainRoutingModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MainModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
          declarations: [_resume_resume_component__WEBPACK_IMPORTED_MODULE_3__["ResumeComponent"], _loading_loading_component__WEBPACK_IMPORTED_MODULE_4__["LoadingComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _main_routing_module__WEBPACK_IMPORTED_MODULE_2__["MainRoutingModule"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/pages/main/resume/resume.component.ts":
  /*!*******************************************************!*\
    !*** ./src/app/pages/main/resume/resume.component.ts ***!
    \*******************************************************/

  /*! exports provided: ResumeComponent */

  /***/
  function srcAppPagesMainResumeResumeComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ResumeComponent", function () {
      return ResumeComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _mockdb_expereince_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ./../../../mockdb/expereince.js */
    "./src/app/mockdb/expereince.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _loading_loading_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../loading/loading.component */
    "./src/app/pages/main/loading/loading.component.ts");

    function ResumeComponent_app_loading_0_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "app-loading");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "123123");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function ResumeComponent_div_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "FULL NAME");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "MR.ANANCHAI SUJIRANUTHAM (P)");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "CURRENT POSITION");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "DEVELOPER (FULL STACK )");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "CAREER OBJECTIVE");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, " Looking for a position ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "developer");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, ", ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "programmer");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, " or related fields. ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "SKILLS");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "h3");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "EXPERT");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, "HTML");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "CSS");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "JAVASCRIPT");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "SQL Database");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "MYSQL Database");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "Oracle Database");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "PHP");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, "CODEIGNITOR");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52, "LARAVEL");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, "BOOTSTRAP");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56, "JQUERY");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, "ANGULAR");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](60, "VUE JS");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](62, "REACT JS");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](64, "ASP.NET C#");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, "ASP.NET VB");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](68, "ASP.NETCORE");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](70, "GIT");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](71, "hr", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "h3");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](73, "NORMAL");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "div", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](76, "RUBY ON RAILS");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](78, "DOCKER");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](80, "Node JS");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "div", 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "div", 9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](83, "EDUCATION");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "div", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](85, " Burapha University, Faculty of Informatics, Software Engineering with grade point 3.21 ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "div", 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "div", 9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](88, "CONTACT");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "div", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "div", 3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "div", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](92, "i", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](93, " \xA0ananchai.sjrnt@gmail.com ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "div", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "div", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](96, "i", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](97, " \xA008-6332-1002 ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "div", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "div", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](101, "div", 9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](102, "FULL NAME");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](103, "div", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](104, "MR.ANANCHAI SUJIRANUTHAM (P)");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "div", 22);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "div", 9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](107, "CURRENT POSITION");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "div", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](109, "DEVELOPER (FULL STACK)");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "div", 23);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](111, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](112, "div", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](113, "div", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](115, "div", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](116, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](117, "div", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](118, "div", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](119, "div", 32);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](120, "div", 33);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](121, "div", 34);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](122, "div", 35);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](123, "div", 9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](124, "SUMMARY OF WORKING EXPERIENCE");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](125, "div", 36);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](126, "div", 37);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](127, "div", 38);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](128, "div", 39);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](129, "i", 40);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](130, "div", 41);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](131, "JUL 2021 - NOW");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](132, "div", 42);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](133, " SA and full stack developer at Fabrinet co. ltd. Getting requirement from user and build program from requirement by use ASP.NET Core, HTML, JS, CSS & Oracle database ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](134, "div", 38);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](135, "div", 39);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](136, "i", 40);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](137, "div", 41);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](138, "OCT 2020 \u2013 JUN 2021");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](139, "div", 42);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](140, " Front end developer at buzzebees co. ltd. (thailand). Coding program from design by designer and connect API to control CRM privileged by use Vue.js & SCSS ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](141, "div", 38);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](142, "div", 39);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](143, "i", 40);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](144, "div", 41);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](145, "AUG 2018 - SEP 2020");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](146, "div", 42);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](147, " IT-Specialist at Celestica (Thailand) Ltd. Full stack developing for control assembling in line and system about e-leaving by use ASP.NET core C#, SQL, HTML, Angular JS , Angular, JS, CSS and jQuery ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](148, "div", 38);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](149, "div", 39);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](150, "i", 40);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](151, "div", 41);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](152, "JAN 2016 - JUL 2018");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](153, "div", 42);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](154, " Software Analysis & Full Stack developer at Siam Compressor Industry Co., Ltd. in Mitsubishi Electric group. My responsibility is setting full loop of SDLC in each system, developing by use ASP.NET (VB or C#), SQL, HTML, JAVASCRIPT, CSS and jQuery ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    var ResumeComponent = /*#__PURE__*/function () {
      function ResumeComponent() {
        _classCallCheck(this, ResumeComponent);

        this.data = _mockdb_expereince_js__WEBPACK_IMPORTED_MODULE_1__["EXPERIENCE"];
        this.isShow = true;
        this.isShowContent = false;
      }

      _createClass(ResumeComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "ngAfterViewInit",
        value: function ngAfterViewInit() {
          var _this = this;

          setTimeout(function () {
            _this.isShow = false;
            _this.isShowContent = true;
          }, 2000);
        }
      }]);

      return ResumeComponent;
    }();

    ResumeComponent.ɵfac = function ResumeComponent_Factory(t) {
      return new (t || ResumeComponent)();
    };

    ResumeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: ResumeComponent,
      selectors: [["app-resume"]],
      decls: 2,
      vars: 2,
      consts: [[4, "ngIf"], ["class", "main-resume", 4, "ngIf"], [1, "main-resume"], [1, "row"], ["id", "section-left", 1, "col-lg-3", "col-md-4", "col-sm-5"], [1, "row", "area-img"], ["src", "./assets/images/unnamed.jpg"], [1, "mobile-description", "pb-4"], [1, "panel"], [1, "panel-header"], [1, "panel-body"], [1, "row", "area-panel", "mt-0"], [1, "row", "skill-items"], [1, "col-lg-6"], [1, "custom-hr"], [1, "col-12"], [1, "fas", "fa-envelope-open-text", "fa-2x", "mt-1"], [1, "row", "mt-1"], [1, "fas", "fa-phone-square-alt", "fa-2x", "mt-1", 2, "margin-left", "2px"], ["id", "section-right", 1, "col-lg-9", "col-md-8", "col-sm-7"], [1, "row", "area-topic"], [1, "panel", "desktop-description"], [1, "panel", "desktop-description", "mt-2"], [1, "line", "desktop-description", "line1", "mt-2"], [1, "dot-line1-1"], [1, "dot-line1-2"], [1, "dot-line1-3"], [1, "line", "desktop-description", "line2"], [1, "dot-line2-1"], [1, "dot-line2-2"], [1, "dot-line2-3"], [1, "line", "desktop-description", "line3"], [1, "dot-line3-1"], [1, "dot-line3-2"], [1, "dot-line3-3"], [1, "panel", "mt-2"], [1, "panel-body", 2, "position", "relative"], [1, "area-timeline", "mt-4"], [1, "timeline-item"], [1, "timeline-dot"], [1, "fas", "fa-history"], [1, "timeline-header"], [1, "timeline-body"]],
      template: function ResumeComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, ResumeComponent_app_loading_0_Template, 2, 0, "app-loading", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ResumeComponent_div_1_Template, 155, 0, "div", 1);
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isShow);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isShowContent);
        }
      },
      directives: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], _loading_loading_component__WEBPACK_IMPORTED_MODULE_3__["LoadingComponent"]],
      styles: [".main-resume[_ngcontent-%COMP%] {\n  padding: 0px 15px;\n  height: 100%;\n}\n\n.main-resume[_ngcontent-%COMP%]    > div[_ngcontent-%COMP%] {\n  height: 100%;\n}\n\n#section-left[_ngcontent-%COMP%] {\n  background-color: #2c3e50;\n  padding-top: 0px;\n  z-index: 1;\n  box-shadow: 7px 0px 30px -8px rgba(0, 0, 0, 0.75);\n}\n\n.desktop-description[_ngcontent-%COMP%] {\n  display: none;\n}\n\n.mobile-description[_ngcontent-%COMP%] {\n  margin-top: 10px;\n  margin-bottom: -10px;\n}\n\n.mobile-description[_ngcontent-%COMP%]    > .panel[_ngcontent-%COMP%] {\n  padding: 5px 10px;\n}\n\n.mobile-description[_ngcontent-%COMP%]    > .panel[_ngcontent-%COMP%]    > .panel-header[_ngcontent-%COMP%] {\n  color: #7f8c8d;\n  font-size: 14px;\n  font-weight: 700;\n}\n\n.mobile-description[_ngcontent-%COMP%]    > .panel[_ngcontent-%COMP%]    > .panel-body[_ngcontent-%COMP%] {\n  color: #fff;\n  font-size: 18px;\n  font-weight: 700;\n}\n\n.area-img[_ngcontent-%COMP%]    > img[_ngcontent-%COMP%] {\n  margin: 0 auto;\n  width: 100%;\n  height: auto;\n}\n\n.area-panel[_ngcontent-%COMP%] {\n  margin-top: 20px;\n}\n\n.area-panel[_ngcontent-%COMP%]    > .panel[_ngcontent-%COMP%] {\n  width: 100%;\n}\n\n.area-panel[_ngcontent-%COMP%]    > .panel[_ngcontent-%COMP%]    > .panel-header[_ngcontent-%COMP%] {\n  box-shadow: 0px 1px 5px 0px #6b696b;\n  background-color: #e67e22;\n  padding: 10px;\n  font-size: 24px;\n  color: #fff;\n  font-weight: 500;\n  letter-spacing: 1.5px;\n  position: relative;\n}\n\n.area-panel[_ngcontent-%COMP%]    > .panel[_ngcontent-%COMP%]    > .panel-header[_ngcontent-%COMP%]::after {\n  content: \" \";\n  position: absolute;\n  left: 10px;\n  top: 50px;\n  width: 0;\n  height: 0;\n  border-left: 15px solid transparent;\n  border-right: 15px solid transparent;\n  border-top: 15px solid #e67e22;\n}\n\n.area-panel[_ngcontent-%COMP%]    > .panel[_ngcontent-%COMP%]    > .panel-body[_ngcontent-%COMP%] {\n  padding: 15px 10px;\n  font-weight: 500;\n  color: #fff;\n  letter-spacing: 1px;\n  font-size: 16px;\n}\n\n.area-panel[_ngcontent-%COMP%]    > .panel[_ngcontent-%COMP%]    > .panel-body[_ngcontent-%COMP%]   b[_ngcontent-%COMP%] {\n  font-weight: 500;\n}\n\n.area-panel[_ngcontent-%COMP%]   .skill-items[_ngcontent-%COMP%] {\n  padding: 0px 15px;\n  padding-top: 0px;\n  margin-top: -20px;\n  margin-right: -10px;\n}\n\n.area-panel[_ngcontent-%COMP%]   .skill-items[_ngcontent-%COMP%]    > div[_ngcontent-%COMP%] {\n  padding: 5px;\n  padding-bottom: 0px;\n  font-size: 14px;\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\n\n.area-panel[_ngcontent-%COMP%]   .skill-items[_ngcontent-%COMP%]    > div[_ngcontent-%COMP%]::before {\n  content: \"-  \";\n}\n\n#section-right[_ngcontent-%COMP%] {\n  background-color: #f5f5f5;\n  padding: 40px;\n  padding-top: 10px;\n}\n\n.area-topic[_ngcontent-%COMP%]    > .panel[_ngcontent-%COMP%] {\n  width: 100%;\n  margin-bottom: 10px;\n}\n\n.area-topic[_ngcontent-%COMP%]    > .panel[_ngcontent-%COMP%]    > .panel-header[_ngcontent-%COMP%] {\n  font-size: 20px;\n  color: #7f8c8d;\n  font-weight: 800;\n  text-transform: uppercase;\n}\n\n.area-topic[_ngcontent-%COMP%]    > .panel[_ngcontent-%COMP%]    > .panel-body[_ngcontent-%COMP%] {\n  font-size: 34px;\n  color: #ff9c00;\n  font-weight: 700;\n  letter-spacing: 1px;\n  margin-top: -12px;\n}\n\n.line[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 8px;\n  background: #2c3e50;\n  margin-bottom: 8px;\n  position: relative;\n}\n\n.line[_ngcontent-%COMP%]    > div[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 0px;\n  right: -20px;\n  width: 20px;\n  height: 10px;\n  z-index: 1;\n  background: #f5f5f5;\n}\n\n.line[_ngcontent-%COMP%]    > .dot-line1-1[_ngcontent-%COMP%] {\n  left: 12%;\n}\n\n.line[_ngcontent-%COMP%]    > .dot-line1-2[_ngcontent-%COMP%] {\n  left: 47%;\n}\n\n.line[_ngcontent-%COMP%]    > .dot-line1-3[_ngcontent-%COMP%] {\n  left: 80%;\n}\n\n.line[_ngcontent-%COMP%]    > .dot-line2-1[_ngcontent-%COMP%] {\n  left: 22%;\n}\n\n.line[_ngcontent-%COMP%]    > .dot-line2-2[_ngcontent-%COMP%] {\n  left: 60%;\n}\n\n.line[_ngcontent-%COMP%]    > .dot-line2-3[_ngcontent-%COMP%] {\n  left: 94%;\n}\n\n.line[_ngcontent-%COMP%]    > .dot-line3-1[_ngcontent-%COMP%] {\n  left: 17%;\n}\n\n.line[_ngcontent-%COMP%]    > .dot-line3-2[_ngcontent-%COMP%] {\n  left: 54%;\n}\n\n.line[_ngcontent-%COMP%]    > .dot-line3-3[_ngcontent-%COMP%] {\n  left: 75%;\n}\n\n.area-timeline[_ngcontent-%COMP%] {\n  margin-top: 40px;\n}\n\n.area-timeline[_ngcontent-%COMP%]::before {\n  content: \" \";\n  background: #e67e22;\n  width: 5px;\n  height: 100%;\n  position: absolute;\n  top: 15px;\n  left: 15px;\n}\n\n.area-timeline[_ngcontent-%COMP%]    > .timeline-item[_ngcontent-%COMP%] {\n  padding-left: 50px;\n  margin-bottom: 30px;\n  position: relative;\n}\n\n.area-timeline[_ngcontent-%COMP%]    > .timeline-item[_ngcontent-%COMP%]    > .timeline-dot[_ngcontent-%COMP%] {\n  padding-top: 2px;\n  border: 3px solid #fff;\n  position: absolute;\n  left: -3px;\n  top: 0;\n  background-color: #e67e22;\n  border-radius: 50%;\n  width: 40px;\n  height: 40px;\n  font-size: 20px;\n  vertical-align: middle;\n  text-align: center;\n  color: #fff;\n  box-shadow: 0px 0px 5px -1px #3d3d3d;\n}\n\n.area-timeline[_ngcontent-%COMP%]    > .timeline-item[_ngcontent-%COMP%]    > .timeline-header[_ngcontent-%COMP%] {\n  color: #2c3e50;\n  font-size: 24px;\n  letter-spacing: 0px;\n  font-weight: 700;\n}\n\n.area-timeline[_ngcontent-%COMP%]    > .timeline-item[_ngcontent-%COMP%]    > .timeline-body[_ngcontent-%COMP%] {\n  margin-left: 15px;\n  color: #2c3e50;\n  font-size: 20px;\n  letter-spacing: 0px;\n  font-weight: 500;\n}\n\nhr[_ngcontent-%COMP%] {\n  border: 1px solid #fff;\n  margin-bottom: 10px;\n  margin-top: 10px;\n}\n\n\n\n@media (min-width: 576px) {\n  .mobile-description[_ngcontent-%COMP%] {\n    display: none;\n  }\n\n  .desktop-description[_ngcontent-%COMP%] {\n    display: inline;\n  }\n  .desktop-description[_ngcontent-%COMP%]   .panel-header[_ngcontent-%COMP%] {\n    font-size: 18px !important;\n  }\n  .desktop-description[_ngcontent-%COMP%]   .panel-body[_ngcontent-%COMP%] {\n    padding-top: 10px;\n    font-size: 28px !important;\n    line-height: 1;\n    font-weight: bold;\n  }\n\n  .hi[_ngcontent-%COMP%] {\n    font-size: 4em;\n  }\n\n  .name[_ngcontent-%COMP%] {\n    font-size: 2em;\n  }\n}\n\n@media (min-width: 768px) {\n  .hi[_ngcontent-%COMP%] {\n    font-size: 5em;\n  }\n\n  .name[_ngcontent-%COMP%] {\n    font-size: 2.4em;\n  }\n}\n\n@media (min-width: 992px) {\n  .hi[_ngcontent-%COMP%] {\n    font-size: 6em;\n  }\n\n  .name[_ngcontent-%COMP%] {\n    font-size: 3em;\n  }\n}\n\n@media (min-width: 1200px) {\n  .hi[_ngcontent-%COMP%] {\n    font-size: 7em;\n  }\n\n  .name[_ngcontent-%COMP%] {\n    font-size: 3.5em;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWFpbi9yZXN1bWUvQzpcXFVzZXJzXFxhbmFuY2hhaXNcXERlc2t0b3BcXEFwcENvZGVcXHJlc3VtZVxccmVzdW1lLWRlbW8vc3JjXFxhcHBcXHBhZ2VzXFxtYWluXFxyZXN1bWVcXHJlc3VtZS5jb21wb25lbnQuc2FzcyIsInNyYy9hcHAvcGFnZXMvbWFpbi9yZXN1bWUvcmVzdW1lLmNvbXBvbmVudC5zYXNzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUdBO0VBQ0ksaUJBQUE7RUFDQSxZQUFBO0FDRko7O0FESUE7RUFDSSxZQUFBO0FDREo7O0FESUE7RUFDSSx5QkFYVTtFQVlWLGdCQUFBO0VBQ0EsVUFBQTtFQUdBLGlEQUFBO0FDREo7O0FESUE7RUFDSSxhQUFBO0FDREo7O0FER0E7RUFDSSxnQkFBQTtFQUNBLG9CQUFBO0FDQUo7O0FERUE7RUFDSSxpQkFBQTtBQ0NKOztBREVBO0VBQ0ksY0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ0NKOztBRENBO0VBQ0ksV0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ0VKOztBRENBO0VBQ0ksY0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FDRUo7O0FEQUE7RUFDSSxnQkFBQTtBQ0dKOztBRERBO0VBQ0ksV0FBQTtBQ0lKOztBREZBO0VBR0ksbUNBQUE7RUFDQSx5QkFBQTtFQUNBLGFBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxrQkFBQTtBQ0tKOztBREhBO0VBQ0ksWUFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLG1DQUFBO0VBQ0Esb0NBQUE7RUFDQSw4QkFBQTtBQ01KOztBREpBO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7QUNPSjs7QURMQTtFQUNJLGdCQUFBO0FDUUo7O0FETkE7RUFDSSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtBQ1NKOztBRFBBO0VBQ0ksWUFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSx1QkFBQTtBQ1VKOztBRFJBO0VBQ0ksY0FBQTtBQ1dKOztBRFRBO0VBQ0kseUJBeEdXO0VBeUdYLGFBQUE7RUFDQSxpQkFBQTtBQ1lKOztBRFZBO0VBQ0ksV0FBQTtFQUNBLG1CQUFBO0FDYUo7O0FEWEE7RUFDSSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EseUJBQUE7QUNjSjs7QURaQTtFQUNJLGVBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FDZUo7O0FEYkE7RUFDSSxXQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQS9IVTtFQWdJVixrQkFBQTtFQUNBLGtCQUFBO0FDZ0JKOztBRGRBO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLG1CQTNJVztBQzRKZjs7QURkQTtFQUNJLFNBQUE7QUNpQko7O0FEZkE7RUFDSSxTQUFBO0FDa0JKOztBRGhCQTtFQUNJLFNBQUE7QUNtQko7O0FEaEJBO0VBQ0ksU0FBQTtBQ21CSjs7QURqQkE7RUFDSSxTQUFBO0FDb0JKOztBRGxCQTtFQUNJLFNBQUE7QUNxQko7O0FEbEJBO0VBQ0ksU0FBQTtBQ3FCSjs7QURuQkE7RUFDSSxTQUFBO0FDc0JKOztBRHBCQTtFQUNJLFNBQUE7QUN1Qko7O0FEcEJBO0VBQ0ksZ0JBQUE7QUN1Qko7O0FEckJBO0VBQ0ksWUFBQTtFQUNBLG1CQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0FDd0JKOztBRHRCQTtFQUNJLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQ3lCSjs7QUR2QkE7RUFDSSxnQkFBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsTUFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUdBLG9DQUFBO0FDMEJKOztBRHZCQTtFQUNJLGNBaE5VO0VBaU5WLGVBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0FDMEJKOztBRHhCQTtFQUNJLGlCQUFBO0VBQ0EsY0F2TlU7RUF3TlYsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QUMyQko7O0FEeEJBO0VBQ0ksc0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0FDMkJKOztBRHZCQSxxQkFBQTs7QUFFQTtFQUNJO0lBQ0ksYUFBQTtFQ3lCTjs7RUR2QkU7SUFDSSxlQUFBO0VDMEJOO0VEekJNO0lBQ0ksMEJBQUE7RUMyQlY7RUR6Qk07SUFDSSxpQkFBQTtJQUNBLDBCQUFBO0lBQ0EsY0FBQTtJQUNBLGlCQUFBO0VDMkJWOztFRHpCRTtJQUNJLGNBQUE7RUM0Qk47O0VEMUJFO0lBQ0ksY0FBQTtFQzZCTjtBQUNGOztBRDFCQTtFQUNJO0lBQ0ksY0FBQTtFQzRCTjs7RUQxQkU7SUFDSSxnQkFBQTtFQzZCTjtBQUNGOztBRDNCQTtFQUNJO0lBQ0ksY0FBQTtFQzZCTjs7RUQzQkU7SUFDSSxjQUFBO0VDOEJOO0FBQ0Y7O0FENUJBO0VBQ0k7SUFDSSxjQUFBO0VDOEJOOztFRDVCRTtJQUNJLGdCQUFBO0VDK0JOO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9tYWluL3Jlc3VtZS9yZXN1bWUuY29tcG9uZW50LnNhc3MiLCJzb3VyY2VzQ29udGVudCI6WyIkY29sb3ItcmlnaHQgOiAjZjVmNWY1XHJcbiRjb2xvci1sZWZ0IDogIzJjM2U1MFxyXG5cclxuLm1haW4tcmVzdW1lXHJcbiAgICBwYWRkaW5nOiAwcHggMTVweFxyXG4gICAgaGVpZ2h0OiAxMDAlXHJcblxyXG4ubWFpbi1yZXN1bWUgPiBkaXZcclxuICAgIGhlaWdodDogMTAwJVxyXG5cclxuXHJcbiNzZWN0aW9uLWxlZnRcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1sZWZ0XHJcbiAgICBwYWRkaW5nLXRvcDogMHB4XHJcbiAgICB6LWluZGV4OiAxXHJcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDdweCAwcHggMzBweCAtOHB4IHJnYmEoMCwwLDAsMC43NSlcclxuICAgIC1tb3otYm94LXNoYWRvdzogN3B4IDBweCAzMHB4IC04cHggcmdiYSgwLDAsMCwwLjc1KVxyXG4gICAgYm94LXNoYWRvdzogN3B4IDBweCAzMHB4IC04cHggcmdiYSgwLDAsMCwwLjc1KVxyXG5cclxuICAgIFxyXG4uZGVza3RvcC1kZXNjcmlwdGlvblxyXG4gICAgZGlzcGxheTogbm9uZVxyXG5cclxuLm1vYmlsZS1kZXNjcmlwdGlvblxyXG4gICAgbWFyZ2luLXRvcDogMTBweFxyXG4gICAgbWFyZ2luLWJvdHRvbTogLTEwcHhcclxuXHJcbi5tb2JpbGUtZGVzY3JpcHRpb24gPiAucGFuZWxcclxuICAgIHBhZGRpbmc6IDVweCAxMHB4XHJcblxyXG5cclxuLm1vYmlsZS1kZXNjcmlwdGlvbiA+IC5wYW5lbCA+IC5wYW5lbC1oZWFkZXJcclxuICAgIGNvbG9yOiAjN2Y4YzhkXHJcbiAgICBmb250LXNpemU6IDE0cHhcclxuICAgIGZvbnQtd2VpZ2h0OiA3MDBcclxuXHJcbi5tb2JpbGUtZGVzY3JpcHRpb24gPiAucGFuZWwgPiAucGFuZWwtYm9keVxyXG4gICAgY29sb3I6ICNmZmZcclxuICAgIGZvbnQtc2l6ZTogMThweFxyXG4gICAgZm9udC13ZWlnaHQ6IDcwMFxyXG5cclxuXHJcbi5hcmVhLWltZyA+IGltZ1xyXG4gICAgbWFyZ2luOiAwIGF1dG9cclxuICAgIHdpZHRoOiAxMDAlXHJcbiAgICBoZWlnaHQ6IGF1dG9cclxuXHJcbi5hcmVhLXBhbmVsXHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4XHJcblxyXG4uYXJlYS1wYW5lbCA+IC5wYW5lbFxyXG4gICAgd2lkdGg6IDEwMCVcclxuXHJcbi5hcmVhLXBhbmVsID4gLnBhbmVsID4gLnBhbmVsLWhlYWRlclxyXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMXB4IDVweCAwcHggcmdiYSgxMDcsMTA1LDEwNywxKVxyXG4gICAgLW1vei1ib3gtc2hhZG93OiAwcHggMXB4IDVweCAwcHggcmdiYSgxMDcsMTA1LDEwNywxKVxyXG4gICAgYm94LXNoYWRvdzogMHB4IDFweCA1cHggMHB4IHJnYmEoMTA3LDEwNSwxMDcsMSlcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNlNjdlMjJcclxuICAgIHBhZGRpbmc6IDEwcHhcclxuICAgIGZvbnQtc2l6ZTogMjRweFxyXG4gICAgY29sb3I6ICNmZmZcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDBcclxuICAgIGxldHRlci1zcGFjaW5nOiAxLjVweFxyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlXHJcblxyXG4uYXJlYS1wYW5lbCA+IC5wYW5lbCA+IC5wYW5lbC1oZWFkZXI6OmFmdGVyXHJcbiAgICBjb250ZW50OiBcIiBcIlxyXG4gICAgcG9zaXRpb246IGFic29sdXRlXHJcbiAgICBsZWZ0OiAxMHB4XHJcbiAgICB0b3A6IDUwcHhcclxuICAgIHdpZHRoOiAwXHJcbiAgICBoZWlnaHQ6IDBcclxuICAgIGJvcmRlci1sZWZ0OiAxNXB4IHNvbGlkIHRyYW5zcGFyZW50XHJcbiAgICBib3JkZXItcmlnaHQ6IDE1cHggc29saWQgdHJhbnNwYXJlbnRcclxuICAgIGJvcmRlci10b3A6IDE1cHggc29saWQgI2U2N2UyMlxyXG5cclxuLmFyZWEtcGFuZWwgPiAucGFuZWwgPiAucGFuZWwtYm9keVxyXG4gICAgcGFkZGluZzogMTVweCAxMHB4ICAgIFxyXG4gICAgZm9udC13ZWlnaHQ6IDUwMFxyXG4gICAgY29sb3I6ICNmZmZcclxuICAgIGxldHRlci1zcGFjaW5nOiAxcHhcclxuICAgIGZvbnQtc2l6ZTogMTZweFxyXG5cclxuLmFyZWEtcGFuZWwgPiAucGFuZWwgPiAucGFuZWwtYm9keSBiXHJcbiAgICBmb250LXdlaWdodDogNTAwXHJcblxyXG4uYXJlYS1wYW5lbCAuc2tpbGwtaXRlbXNcclxuICAgIHBhZGRpbmc6IDBweCAxNXB4XHJcbiAgICBwYWRkaW5nLXRvcDogMHB4XHJcbiAgICBtYXJnaW4tdG9wOiAtMjBweFxyXG4gICAgbWFyZ2luLXJpZ2h0OiAtMTBweFxyXG5cclxuLmFyZWEtcGFuZWwgLnNraWxsLWl0ZW1zID4gZGl2XHJcbiAgICBwYWRkaW5nOiA1cHhcclxuICAgIHBhZGRpbmctYm90dG9tOiAwcHhcclxuICAgIGZvbnQtc2l6ZTogMTRweFxyXG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcFxyXG4gICAgb3ZlcmZsb3c6IGhpZGRlblxyXG4gICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXNcclxuXHJcbi5hcmVhLXBhbmVsIC5za2lsbC1pdGVtcyA+IGRpdjo6YmVmb3JlXHJcbiAgICBjb250ZW50OiBcIi0gIFwiXHJcblxyXG4jc2VjdGlvbi1yaWdodFxyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLXJpZ2h0XHJcbiAgICBwYWRkaW5nOiA0MHB4XHJcbiAgICBwYWRkaW5nLXRvcDogMTBweFxyXG5cclxuLmFyZWEtdG9waWMgPiAucGFuZWxcclxuICAgIHdpZHRoOiAxMDAlXHJcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4XHJcblxyXG4uYXJlYS10b3BpYyA+IC5wYW5lbCA+IC5wYW5lbC1oZWFkZXJcclxuICAgIGZvbnQtc2l6ZTogMjBweFxyXG4gICAgY29sb3I6ICM3ZjhjOGRcclxuICAgIGZvbnQtd2VpZ2h0OiA4MDBcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2VcclxuXHJcbi5hcmVhLXRvcGljID4gLnBhbmVsID4gLnBhbmVsLWJvZHlcclxuICAgIGZvbnQtc2l6ZTogMzRweFxyXG4gICAgY29sb3I6ICNmZjljMDBcclxuICAgIGZvbnQtd2VpZ2h0OiA3MDBcclxuICAgIGxldHRlci1zcGFjaW5nOiAxcHhcclxuICAgIG1hcmdpbi10b3A6IC0xMnB4XHJcblxyXG4ubGluZVxyXG4gICAgd2lkdGg6IDEwMCVcclxuICAgIGhlaWdodDogOHB4XHJcbiAgICBiYWNrZ3JvdW5kOiAkY29sb3ItbGVmdFxyXG4gICAgbWFyZ2luLWJvdHRvbTogOHB4XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmVcclxuICAgIFxyXG4ubGluZSA+IGRpdiBcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZVxyXG4gICAgdG9wOiAwcHhcclxuICAgIHJpZ2h0OiAtMjBweFxyXG4gICAgd2lkdGg6IDIwcHhcclxuICAgIGhlaWdodDogMTBweFxyXG4gICAgei1pbmRleDogMVxyXG4gICAgYmFja2dyb3VuZDogJGNvbG9yLXJpZ2h0XHJcblxyXG5cclxuLmxpbmUgPiAuZG90LWxpbmUxLTFcclxuICAgIGxlZnQ6IDEyJVxyXG5cclxuLmxpbmUgPiAuZG90LWxpbmUxLTJcclxuICAgIGxlZnQ6IDQ3JVxyXG5cclxuLmxpbmUgPiAuZG90LWxpbmUxLTNcclxuICAgIGxlZnQ6IDgwJVxyXG5cclxuICAgIFxyXG4ubGluZSA+IC5kb3QtbGluZTItMVxyXG4gICAgbGVmdDogMjIlXHJcblxyXG4ubGluZSA+IC5kb3QtbGluZTItMlxyXG4gICAgbGVmdDogNjAlXHJcblxyXG4ubGluZSA+IC5kb3QtbGluZTItM1xyXG4gICAgbGVmdDogOTQlXHJcblxyXG5cclxuLmxpbmUgPiAuZG90LWxpbmUzLTFcclxuICAgIGxlZnQ6IDE3JVxyXG5cclxuLmxpbmUgPiAuZG90LWxpbmUzLTJcclxuICAgIGxlZnQ6IDU0JVxyXG5cclxuLmxpbmUgPiAuZG90LWxpbmUzLTNcclxuICAgIGxlZnQ6IDc1JVxyXG5cclxuXHJcbi5hcmVhLXRpbWVsaW5lXHJcbiAgICBtYXJnaW4tdG9wOiA0MHB4XHJcblxyXG4uYXJlYS10aW1lbGluZTo6YmVmb3JlXHJcbiAgICBjb250ZW50OiBcIiBcIlxyXG4gICAgYmFja2dyb3VuZDogI2U2N2UyMlxyXG4gICAgd2lkdGg6IDVweFxyXG4gICAgaGVpZ2h0OiAxMDAlXHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGVcclxuICAgIHRvcDogMTVweFxyXG4gICAgbGVmdDogMTVweFxyXG5cclxuLmFyZWEtdGltZWxpbmUgPiAudGltZWxpbmUtaXRlbVxyXG4gICAgcGFkZGluZy1sZWZ0OiA1MHB4XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzMHB4XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmVcclxuXHJcbi5hcmVhLXRpbWVsaW5lID4gLnRpbWVsaW5lLWl0ZW0gPiAudGltZWxpbmUtZG90XHJcbiAgICBwYWRkaW5nLXRvcDogMnB4XHJcbiAgICBib3JkZXI6IDNweCBzb2xpZCAjZmZmXHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGVcclxuICAgIGxlZnQ6IC0zcHhcclxuICAgIHRvcDogMFxyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2U2N2UyMlxyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlXHJcbiAgICB3aWR0aDogNDBweFxyXG4gICAgaGVpZ2h0OiA0MHB4XHJcbiAgICBmb250LXNpemU6IDIwcHhcclxuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGVcclxuICAgIHRleHQtYWxpZ246IGNlbnRlclxyXG4gICAgY29sb3I6ICNmZmZcclxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDBweCA1cHggLTFweCByZ2JhKDYxLDYxLDYxLDEpXHJcbiAgICAtbW96LWJveC1zaGFkb3c6IDBweCAwcHggNXB4IC0xcHggcmdiYSg2MSw2MSw2MSwxKVxyXG4gICAgYm94LXNoYWRvdzogMHB4IDBweCA1cHggLTFweCByZ2JhKDYxLDYxLDYxLDEpXHJcblxyXG5cclxuLmFyZWEtdGltZWxpbmUgPiAudGltZWxpbmUtaXRlbSA+IC50aW1lbGluZS1oZWFkZXJcclxuICAgIGNvbG9yOiAkY29sb3ItbGVmdFxyXG4gICAgZm9udC1zaXplOiAyNHB4XHJcbiAgICBsZXR0ZXItc3BhY2luZzogMHB4XHJcbiAgICBmb250LXdlaWdodDogNzAwXHJcblxyXG4uYXJlYS10aW1lbGluZSA+IC50aW1lbGluZS1pdGVtID4gLnRpbWVsaW5lLWJvZHlcclxuICAgIG1hcmdpbi1sZWZ0OiAxNXB4XHJcbiAgICBjb2xvcjogJGNvbG9yLWxlZnRcclxuICAgIGZvbnQtc2l6ZTogMjBweFxyXG4gICAgbGV0dGVyLXNwYWNpbmc6IDBweFxyXG4gICAgZm9udC13ZWlnaHQ6IDUwMFxyXG5cclxuXHJcbmhyXHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZmZmXHJcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4XHJcblxyXG5cclxuIFxyXG4vKiBjb250cm9sIGZvbnQgc2l6ZSovXHJcbi8vIFNtYWxsIGRldmljZXMgKGxhbmRzY2FwZSBwaG9uZXMsIDU3NnB4IGFuZCB1cClcclxuQG1lZGlhIChtaW4td2lkdGg6IDU3NnB4KSBcclxuICAgIC5tb2JpbGUtZGVzY3JpcHRpb25cclxuICAgICAgICBkaXNwbGF5OiBub25lXHJcblxyXG4gICAgLmRlc2t0b3AtZGVzY3JpcHRpb25cclxuICAgICAgICBkaXNwbGF5OiBpbmxpbmVcclxuICAgICAgICAucGFuZWwtaGVhZGVyXHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMThweCAhaW1wb3J0YW50XHJcblxyXG4gICAgICAgIC5wYW5lbC1ib2R5IFxyXG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMTBweFxyXG4gICAgICAgICAgICBmb250LXNpemU6IDI4cHggIWltcG9ydGFudFxyXG4gICAgICAgICAgICBsaW5lLWhlaWdodDogMVxyXG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZFxyXG5cclxuICAgIC5oaVxyXG4gICAgICAgIGZvbnQtc2l6ZTogNGVtXHJcblxyXG4gICAgLm5hbWUgXHJcbiAgICAgICAgZm9udC1zaXplOiAyZW1cclxuICAgIFxyXG5cclxuLy8gTWVkaXVtIGRldmljZXMgKHRhYmxldHMsIDc2OHB4IGFuZCB1cClcclxuQG1lZGlhIChtaW4td2lkdGg6IDc2OHB4KVxyXG4gICAgLmhpXHJcbiAgICAgICAgZm9udC1zaXplOiA1ZW1cclxuXHJcbiAgICAubmFtZSBcclxuICAgICAgICBmb250LXNpemU6IDIuNGVtXHJcblxyXG4vLyBMYXJnZSBkZXZpY2VzIChkZXNrdG9wcywgOTkycHggYW5kIHVwKVxyXG5AbWVkaWEgKG1pbi13aWR0aDogOTkycHgpIFxyXG4gICAgLmhpXHJcbiAgICAgICAgZm9udC1zaXplOiA2ZW1cclxuXHJcbiAgICAubmFtZVxyXG4gICAgICAgIGZvbnQtc2l6ZTogM2VtXHJcblxyXG4vLyBFeHRyYSBsYXJnZSBkZXZpY2VzIChsYXJnZSBkZXNrdG9wcywgMTIwMHB4IGFuZCB1cClcclxuQG1lZGlhIChtaW4td2lkdGg6IDEyMDBweCkgXHJcbiAgICAuaGlcclxuICAgICAgICBmb250LXNpemU6IDdlbVxyXG5cclxuICAgIC5uYW1lXHJcbiAgICAgICAgZm9udC1zaXplOiAzLjVlbVxyXG5cclxuIiwiLm1haW4tcmVzdW1lIHtcbiAgcGFkZGluZzogMHB4IDE1cHg7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuLm1haW4tcmVzdW1lID4gZGl2IHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4jc2VjdGlvbi1sZWZ0IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzJjM2U1MDtcbiAgcGFkZGluZy10b3A6IDBweDtcbiAgei1pbmRleDogMTtcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiA3cHggMHB4IDMwcHggLThweCByZ2JhKDAsIDAsIDAsIDAuNzUpO1xuICAtbW96LWJveC1zaGFkb3c6IDdweCAwcHggMzBweCAtOHB4IHJnYmEoMCwgMCwgMCwgMC43NSk7XG4gIGJveC1zaGFkb3c6IDdweCAwcHggMzBweCAtOHB4IHJnYmEoMCwgMCwgMCwgMC43NSk7XG59XG5cbi5kZXNrdG9wLWRlc2NyaXB0aW9uIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cblxuLm1vYmlsZS1kZXNjcmlwdGlvbiB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIG1hcmdpbi1ib3R0b206IC0xMHB4O1xufVxuXG4ubW9iaWxlLWRlc2NyaXB0aW9uID4gLnBhbmVsIHtcbiAgcGFkZGluZzogNXB4IDEwcHg7XG59XG5cbi5tb2JpbGUtZGVzY3JpcHRpb24gPiAucGFuZWwgPiAucGFuZWwtaGVhZGVyIHtcbiAgY29sb3I6ICM3ZjhjOGQ7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbn1cblxuLm1vYmlsZS1kZXNjcmlwdGlvbiA+IC5wYW5lbCA+IC5wYW5lbC1ib2R5IHtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbn1cblxuLmFyZWEtaW1nID4gaW1nIHtcbiAgbWFyZ2luOiAwIGF1dG87XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IGF1dG87XG59XG5cbi5hcmVhLXBhbmVsIHtcbiAgbWFyZ2luLXRvcDogMjBweDtcbn1cblxuLmFyZWEtcGFuZWwgPiAucGFuZWwge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmFyZWEtcGFuZWwgPiAucGFuZWwgPiAucGFuZWwtaGVhZGVyIHtcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMXB4IDVweCAwcHggIzZiNjk2YjtcbiAgLW1vei1ib3gtc2hhZG93OiAwcHggMXB4IDVweCAwcHggIzZiNjk2YjtcbiAgYm94LXNoYWRvdzogMHB4IDFweCA1cHggMHB4ICM2YjY5NmI7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlNjdlMjI7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGZvbnQtc2l6ZTogMjRweDtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGxldHRlci1zcGFjaW5nOiAxLjVweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4uYXJlYS1wYW5lbCA+IC5wYW5lbCA+IC5wYW5lbC1oZWFkZXI6OmFmdGVyIHtcbiAgY29udGVudDogXCIgXCI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMTBweDtcbiAgdG9wOiA1MHB4O1xuICB3aWR0aDogMDtcbiAgaGVpZ2h0OiAwO1xuICBib3JkZXItbGVmdDogMTVweCBzb2xpZCB0cmFuc3BhcmVudDtcbiAgYm9yZGVyLXJpZ2h0OiAxNXB4IHNvbGlkIHRyYW5zcGFyZW50O1xuICBib3JkZXItdG9wOiAxNXB4IHNvbGlkICNlNjdlMjI7XG59XG5cbi5hcmVhLXBhbmVsID4gLnBhbmVsID4gLnBhbmVsLWJvZHkge1xuICBwYWRkaW5nOiAxNXB4IDEwcHg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGNvbG9yOiAjZmZmO1xuICBsZXR0ZXItc3BhY2luZzogMXB4O1xuICBmb250LXNpemU6IDE2cHg7XG59XG5cbi5hcmVhLXBhbmVsID4gLnBhbmVsID4gLnBhbmVsLWJvZHkgYiB7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG5cbi5hcmVhLXBhbmVsIC5za2lsbC1pdGVtcyB7XG4gIHBhZGRpbmc6IDBweCAxNXB4O1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBtYXJnaW4tdG9wOiAtMjBweDtcbiAgbWFyZ2luLXJpZ2h0OiAtMTBweDtcbn1cblxuLmFyZWEtcGFuZWwgLnNraWxsLWl0ZW1zID4gZGl2IHtcbiAgcGFkZGluZzogNXB4O1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xuICBmb250LXNpemU6IDE0cHg7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xufVxuXG4uYXJlYS1wYW5lbCAuc2tpbGwtaXRlbXMgPiBkaXY6OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiLSAgXCI7XG59XG5cbiNzZWN0aW9uLXJpZ2h0IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y1ZjVmNTtcbiAgcGFkZGluZzogNDBweDtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG59XG5cbi5hcmVhLXRvcGljID4gLnBhbmVsIHtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG5cbi5hcmVhLXRvcGljID4gLnBhbmVsID4gLnBhbmVsLWhlYWRlciB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgY29sb3I6ICM3ZjhjOGQ7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG5cbi5hcmVhLXRvcGljID4gLnBhbmVsID4gLnBhbmVsLWJvZHkge1xuICBmb250LXNpemU6IDM0cHg7XG4gIGNvbG9yOiAjZmY5YzAwO1xuICBmb250LXdlaWdodDogNzAwO1xuICBsZXR0ZXItc3BhY2luZzogMXB4O1xuICBtYXJnaW4tdG9wOiAtMTJweDtcbn1cblxuLmxpbmUge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiA4cHg7XG4gIGJhY2tncm91bmQ6ICMyYzNlNTA7XG4gIG1hcmdpbi1ib3R0b206IDhweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4ubGluZSA+IGRpdiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwcHg7XG4gIHJpZ2h0OiAtMjBweDtcbiAgd2lkdGg6IDIwcHg7XG4gIGhlaWdodDogMTBweDtcbiAgei1pbmRleDogMTtcbiAgYmFja2dyb3VuZDogI2Y1ZjVmNTtcbn1cblxuLmxpbmUgPiAuZG90LWxpbmUxLTEge1xuICBsZWZ0OiAxMiU7XG59XG5cbi5saW5lID4gLmRvdC1saW5lMS0yIHtcbiAgbGVmdDogNDclO1xufVxuXG4ubGluZSA+IC5kb3QtbGluZTEtMyB7XG4gIGxlZnQ6IDgwJTtcbn1cblxuLmxpbmUgPiAuZG90LWxpbmUyLTEge1xuICBsZWZ0OiAyMiU7XG59XG5cbi5saW5lID4gLmRvdC1saW5lMi0yIHtcbiAgbGVmdDogNjAlO1xufVxuXG4ubGluZSA+IC5kb3QtbGluZTItMyB7XG4gIGxlZnQ6IDk0JTtcbn1cblxuLmxpbmUgPiAuZG90LWxpbmUzLTEge1xuICBsZWZ0OiAxNyU7XG59XG5cbi5saW5lID4gLmRvdC1saW5lMy0yIHtcbiAgbGVmdDogNTQlO1xufVxuXG4ubGluZSA+IC5kb3QtbGluZTMtMyB7XG4gIGxlZnQ6IDc1JTtcbn1cblxuLmFyZWEtdGltZWxpbmUge1xuICBtYXJnaW4tdG9wOiA0MHB4O1xufVxuXG4uYXJlYS10aW1lbGluZTo6YmVmb3JlIHtcbiAgY29udGVudDogXCIgXCI7XG4gIGJhY2tncm91bmQ6ICNlNjdlMjI7XG4gIHdpZHRoOiA1cHg7XG4gIGhlaWdodDogMTAwJTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDE1cHg7XG4gIGxlZnQ6IDE1cHg7XG59XG5cbi5hcmVhLXRpbWVsaW5lID4gLnRpbWVsaW5lLWl0ZW0ge1xuICBwYWRkaW5nLWxlZnQ6IDUwcHg7XG4gIG1hcmdpbi1ib3R0b206IDMwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLmFyZWEtdGltZWxpbmUgPiAudGltZWxpbmUtaXRlbSA+IC50aW1lbGluZS1kb3Qge1xuICBwYWRkaW5nLXRvcDogMnB4O1xuICBib3JkZXI6IDNweCBzb2xpZCAjZmZmO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IC0zcHg7XG4gIHRvcDogMDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U2N2UyMjtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICB3aWR0aDogNDBweDtcbiAgaGVpZ2h0OiA0MHB4O1xuICBmb250LXNpemU6IDIwcHg7XG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6ICNmZmY7XG4gIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDBweCA1cHggLTFweCAjM2QzZDNkO1xuICAtbW96LWJveC1zaGFkb3c6IDBweCAwcHggNXB4IC0xcHggIzNkM2QzZDtcbiAgYm94LXNoYWRvdzogMHB4IDBweCA1cHggLTFweCAjM2QzZDNkO1xufVxuXG4uYXJlYS10aW1lbGluZSA+IC50aW1lbGluZS1pdGVtID4gLnRpbWVsaW5lLWhlYWRlciB7XG4gIGNvbG9yOiAjMmMzZTUwO1xuICBmb250LXNpemU6IDI0cHg7XG4gIGxldHRlci1zcGFjaW5nOiAwcHg7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG59XG5cbi5hcmVhLXRpbWVsaW5lID4gLnRpbWVsaW5lLWl0ZW0gPiAudGltZWxpbmUtYm9keSB7XG4gIG1hcmdpbi1sZWZ0OiAxNXB4O1xuICBjb2xvcjogIzJjM2U1MDtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBsZXR0ZXItc3BhY2luZzogMHB4O1xuICBmb250LXdlaWdodDogNTAwO1xufVxuXG5ociB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmZmY7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG5cbi8qIGNvbnRyb2wgZm9udCBzaXplKi9cbkBtZWRpYSAobWluLXdpZHRoOiA1NzZweCkge1xuICAubW9iaWxlLWRlc2NyaXB0aW9uIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG5cbiAgLmRlc2t0b3AtZGVzY3JpcHRpb24ge1xuICAgIGRpc3BsYXk6IGlubGluZTtcbiAgfVxuICAuZGVza3RvcC1kZXNjcmlwdGlvbiAucGFuZWwtaGVhZGVyIHtcbiAgICBmb250LXNpemU6IDE4cHggIWltcG9ydGFudDtcbiAgfVxuICAuZGVza3RvcC1kZXNjcmlwdGlvbiAucGFuZWwtYm9keSB7XG4gICAgcGFkZGluZy10b3A6IDEwcHg7XG4gICAgZm9udC1zaXplOiAyOHB4ICFpbXBvcnRhbnQ7XG4gICAgbGluZS1oZWlnaHQ6IDE7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIH1cblxuICAuaGkge1xuICAgIGZvbnQtc2l6ZTogNGVtO1xuICB9XG5cbiAgLm5hbWUge1xuICAgIGZvbnQtc2l6ZTogMmVtO1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcbiAgLmhpIHtcbiAgICBmb250LXNpemU6IDVlbTtcbiAgfVxuXG4gIC5uYW1lIHtcbiAgICBmb250LXNpemU6IDIuNGVtO1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogOTkycHgpIHtcbiAgLmhpIHtcbiAgICBmb250LXNpemU6IDZlbTtcbiAgfVxuXG4gIC5uYW1lIHtcbiAgICBmb250LXNpemU6IDNlbTtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDEyMDBweCkge1xuICAuaGkge1xuICAgIGZvbnQtc2l6ZTogN2VtO1xuICB9XG5cbiAgLm5hbWUge1xuICAgIGZvbnQtc2l6ZTogMy41ZW07XG4gIH1cbn0iXX0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ResumeComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-resume',
          templateUrl: './resume.component.html',
          styleUrls: ['./resume.component.sass']
        }]
      }], function () {
        return [];
      }, null);
    })();
    /***/

  }
}]);
//# sourceMappingURL=pages-main-main-module-es5.js.map